package br.com.zup.ZupAgenda.controllers;

import br.com.zup.ZupAgenda.dtos.CadastroContatoDTO;
import br.com.zup.ZupAgenda.models.Contato;
import br.com.zup.ZupAgenda.services.ContatoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/contatos")
public class ContatoController {
    @Autowired
    private ContatoService contatoService;

    @RequestMapping(method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public Contato cadastrarContato(@RequestBody @Valid CadastroContatoDTO contatoDTO){
        Contato contatoModel = contatoDTO.converterDTOemContato();
        return contatoService.cadastrarContato(contatoModel);
    }

    @GetMapping
    public Iterable<Contato> visualizarContatos(){
        return contatoService.exibirContatos();
    }

    @PutMapping("/contato/{id}")
    public Contato atualizarContato(@RequestBody CadastroContatoDTO contatoDTO, @PathVariable int id){
        Contato contatoModel = contatoDTO.converterDTOemContato();
        Contato contatoDoBanco;
        contatoDoBanco = contatoService.pesquisaContato(id);
        return this.contatoService.alterarContato(contatoModel, contatoDoBanco);
    }

    @DeleteMapping("/contato/{id}")
    public void deletarContato(@PathVariable int id){
        this.contatoService.deletarContato(id);
    }

    @GetMapping("/{letra}")
    public List<Contato> pesquisarPorNome(@PathVariable String letra){
        return this.contatoService.pesquisaPorLetra(letra);
    }

}

package br.com.zup.ZupAgenda.services;

import br.com.zup.ZupAgenda.models.Contato;
import br.com.zup.ZupAgenda.repositories.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ContatoService {
    @Autowired
    private ContatoRepository contatoRepository;

    public Contato cadastrarContato(Contato contato){
        return contatoRepository.save(contato);
    }

    public Iterable<Contato> exibirContatos(){
        return contatoRepository.findAll();

        /*
        List<Contato> contatos = new ArrayList<>();
        contatoRepository.findAll().iterator().forEachRemaining(contatos::add);
         */
    }

    public Contato pesquisaContato(int id){
        return contatoRepository.findById(id).get();
    }

    public Contato alterarContato(Contato contatoAtualizacao, Contato contatoBancoDados){
        this.contatoRepository.save(contatoBancoDados).setNome(contatoAtualizacao.getNome());
        this.contatoRepository.save(contatoBancoDados).setEmail(contatoAtualizacao.getEmail());
        this.contatoRepository.save(contatoBancoDados).setTelefone(contatoAtualizacao.getTelefone());

        return this.contatoRepository.save(contatoBancoDados);
    }

    public void deletarContato(int id){
        this.contatoRepository.deleteById(id);
    }

    public List<Contato> pesquisaPorLetra(String letra){
        return this.contatoRepository.findByFirstnameStartingWith(letra);
    }

}
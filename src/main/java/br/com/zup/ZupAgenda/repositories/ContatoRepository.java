package br.com.zup.ZupAgenda.repositories;

import br.com.zup.ZupAgenda.models.Contato;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ContatoRepository extends CrudRepository<Contato, Integer> {

    @Query("select c from Contato c where c.nome like ?1%")
    List<Contato> findByFirstnameStartingWith(String letra);

}

package br.com.zup.ZupAgenda.models;

import javax.persistence.*;

//Mapeamento do BD - Cria o banco, caso não haja, de acordo com a configuração properties

@Entity
@Table(name = "contatos")
public class Contato {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) //deixar tipo de entidade, deixando auto incremento
    private int id;
    private String nome;
    @Column(unique = true, nullable = false) // dizer que ele é unico e não pode ser nula
    private String email;
    private String telefone;

    public Contato() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return "Contato{" +
                "id=" + id +
                ", nome='" + nome + '\'' +
                ", email='" + email + '\'' +
                ", telefone='" + telefone + '\'' +
                '}';
    }
}
